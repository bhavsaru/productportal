# ProductPortal

Pre-requisites for thie project

Node.js

npm

ng 

Google chrome to run unit test cases

git 

to insatll git follow instructions on this site:

https://git-scm.com/book/en/v2/Getting-Started-Installing-Git

to install Node.js: https://nodejs.org/en/

npm will be installed by Node.js 

to install angular CLI run below command after npm is insatlled:

npm install -g @angular/cli

checkout the project from GIT repo: https://bhavsaru@bitbucket.org/bhavsaru/productportal.git

command: git clone https://bhavsaru@bitbucket.org/bhavsaru/productportal.git

go to the base directory of the checkedout project: 

run below command to install all the dependencies: 

npm install

run below command to run the unit test cases

ng test --code-coverage

it will generate the unit test code coverage at ProductPortal/coverage/ProductPortal/index.html

to see the ProductPortal running on browser:

ng serve --open





