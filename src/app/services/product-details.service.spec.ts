import { TestBed } from '@angular/core/testing';

import { ProductDetailsService } from './product-details.service';

describe('ProductDetailsService', () => {
  let service: ProductDetailsService;

  beforeEach(() =>
  {
    TestBed.configureTestingModule({});
    service = TestBed.get(ProductDetailsService);
  });


  it('should be created', () => {
    expect(service).toBeTruthy();
  });


  it('should have getProductDetails method', () => {
  expect(service.getProductDetails).toBeTruthy();
  });

  it('should call getProductDetails method', () => {

    spyOn(service, 'getProductDetails').and.callThrough();
    service.getProductDetails();
    expect(service.getProductDetails).toHaveBeenCalledWith();
  });

});
