import { Component } from '@angular/core';
import {ProductDetailsService} from "./services/product-details.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'ProductPortal';
  productInfo : any;


  constructor(private productDetailsService: ProductDetailsService) {
  }


  ngOnInit() {
   this.productInfo = this.getProductDetails();
  }

  getProductDetails() {
    return this.productDetailsService.getProductDetails();
  }


}
