import { Component, OnInit, Input } from '@angular/core';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-product-carousal',
  templateUrl: './product-carousal.component.html',
  styleUrls: ['./product-carousal.component.scss']
})
export class ProductCarousalComponent implements OnInit {

  @Input() product: any;


  images: string[] = [];

  constructor(public activeModal: NgbActiveModal) {
  }

  ngOnInit() {
    if(this.product) {
      this.images.push(this.product.hero.href);

      this.product.images.forEach((image) => {this.images.push(image.href)});
    }

  }

}
