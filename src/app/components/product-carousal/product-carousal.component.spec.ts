import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { NgbModule,NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ProductCarousalComponent } from './product-carousal.component';
import {isNotNullOrUndefined} from "codelyzer/util/isNotNullOrUndefined";

describe('ProductCarousalComponent', () => {
  let component: ProductCarousalComponent;
  let fixture: ComponentFixture<ProductCarousalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [NgbModule],
      declarations: [ ProductCarousalComponent ],
      providers: [NgbActiveModal]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductCarousalComponent);
    component = fixture.componentInstance;
    component.product = {
      "id":"clipped-jacquard-squares-duvet-cover-shams-t5416",
      "name":"Clipped Jacquard Squares Duvet Cover &amp; Shams",
      "links":{
        "www":"https://www.westelm.com/products/clipped-jacquard-squares-duvet-cover-shams-t5416/"
      },
      "priceRange":{
        "regular":{
          "high":199,
          "low":34
        },
        "selling":{
          "high":149.25,
          "low":25.5
        },
        "type":"special"
      },
      "thumbnail":{
        "size":"m",
        "meta":"",
        "alt":"",
        "rel":"thumbnail",
        "width":363,
        "href":"https://www.westelm.com/weimgs/rk/images/wcm/products/201951/0017/clipped-jacquard-squares-duvet-cover-shams-m.jpg",
        "height":363
      },
      "hero":{
        "size":"m",
        "meta":"",
        "alt":"",
        "rel":"hero",
        "width":363,
        "href":"https://www.westelm.com/weimgs/rk/images/wcm/products/201951/0017/clipped-jacquard-squares-duvet-cover-shams-m.jpg",
        "height":363
      },
      "images":[
        {
          "size":"m",
          "meta":"",
          "alt":"",
          "rel":"althero",
          "width":363,
          "href":"https://www.westelm.com/weimgs/rk/images/wcm/products/201951/0018/clipped-jacquard-squares-duvet-cover-shams-m.jpg",
          "height":363
        },
        {
          "size":"m",
          "meta":"",
          "alt":"",
          "rel":"althero",
          "width":363,
          "href":"https://www.westelm.com/weimgs/rk/images/wcm/products/201952/0009/clipped-jacquard-squares-duvet-cover-shams-m.jpg",
          "height":363
        }
      ],
      "swatches":[

      ],
      "messages":[
        "One Day Bonus Deal!"
      ],
      "flags":[
        {
          "bopisSuppress":false,
          "rank":3,
          "id":"newcore"
        },
        {
          "bopisSuppress":false,
          "rank":7,
          "id":"organic"
        },
        {
          "bopisSuppress":false,
          "rank":9,
          "id":"fairTrade"
        }
      ],
      "reviews":{
        "recommendationCount":0,
        "likelihood":0,
        "reviewCount":0,
        "averageRating":0,
        "id":"clipped-jacquard-squares-duvet-cover-shams-t5416",
        "type":"GROUP_REVIEWS"
      }
    };
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have ngOnInit method', () => {
    expect(component.ngOnInit).toBeTruthy();
  });


  it('should call ngOnInit method when product is defined', () => {
    component.images = [];
    component.ngOnInit();
    expect(component.images.length).toBe(3);
  });

});


describe('ProductCarousalComponent', () => {
  let component: ProductCarousalComponent;
  let fixture: ComponentFixture<ProductCarousalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
        imports: [NgbModule],
        declarations: [ ProductCarousalComponent ],
        providers: [NgbActiveModal]
      })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductCarousalComponent);
    component = fixture.componentInstance;
    component.product = undefined;
    fixture.detectChanges();
  });



  it('should call ngOnInit method when product is undefined', () => {
    component.images = [];
    component.ngOnInit();
    expect(component.images.length).toBe(0);
  });

});
