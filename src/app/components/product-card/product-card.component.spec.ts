import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ProductCardComponent } from './product-card.component';
import {ProductCarousalComponent} from "../product-carousal/product-carousal.component";
import {BrowserDynamicTestingModule} from '@angular/platform-browser-dynamic/testing';

describe('ProductCardComponent', () => {
  let component: ProductCardComponent;
  let fixture: ComponentFixture<ProductCardComponent>;
  let modalService: NgbModal;
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [NgbModule],
      declarations: [ ProductCardComponent, ProductCarousalComponent ],
      providers: [NgbModal]
    }).overrideModule(BrowserDynamicTestingModule, { set: { entryComponents: [ProductCarousalComponent] } })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductCardComponent);
    component = fixture.componentInstance;
    component.product =  {
      "id":"clipped-jacquard-squares-duvet-cover-shams-t5416",
      "name":"Clipped Jacquard Squares Duvet Cover &amp; Shams",
      "links":{
        "www":"https://www.westelm.com/products/clipped-jacquard-squares-duvet-cover-shams-t5416/"
      },
      "priceRange":{
        "regular":{
          "high":199,
          "low":34
        },
        "selling":{
          "high":149.25,
          "low":25.5
        },
        "type":"special"
      },
      "thumbnail":{
        "size":"m",
        "meta":"",
        "alt":"",
        "rel":"thumbnail",
        "width":363,
        "href":"https://www.westelm.com/weimgs/rk/images/wcm/products/201951/0017/clipped-jacquard-squares-duvet-cover-shams-m.jpg",
        "height":363
      },
      "hero":{
        "size":"m",
        "meta":"",
        "alt":"",
        "rel":"hero",
        "width":363,
        "href":"https://www.westelm.com/weimgs/rk/images/wcm/products/201951/0017/clipped-jacquard-squares-duvet-cover-shams-m.jpg",
        "height":363
      },
      "images":[
        {
          "size":"m",
          "meta":"",
          "alt":"",
          "rel":"althero",
          "width":363,
          "href":"https://www.westelm.com/weimgs/rk/images/wcm/products/201951/0018/clipped-jacquard-squares-duvet-cover-shams-m.jpg",
          "height":363
        },
        {
          "size":"m",
          "meta":"",
          "alt":"",
          "rel":"althero",
          "width":363,
          "href":"https://www.westelm.com/weimgs/rk/images/wcm/products/201952/0009/clipped-jacquard-squares-duvet-cover-shams-m.jpg",
          "height":363
        }
      ],
      "swatches":[

      ],
      "messages":[
        "One Day Bonus Deal!"
      ],
      "flags":[
        {
          "bopisSuppress":false,
          "rank":3,
          "id":"newcore"
        },
        {
          "bopisSuppress":false,
          "rank":7,
          "id":"organic"
        },
        {
          "bopisSuppress":false,
          "rank":9,
          "id":"fairTrade"
        }
      ],
      "reviews":{
        "recommendationCount":0,
        "likelihood":0,
        "reviewCount":0,
        "averageRating":0,
        "id":"clipped-jacquard-squares-duvet-cover-shams-t5416",
        "type":"GROUP_REVIEWS"
      }
    };

    modalService = TestBed.get(NgbModal);
    fixture.detectChanges();


    spyOn(modalService, 'open').and.callThrough();

  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have open method', () => {
    expect(component.open).toBeTruthy();
  });
  it('should call open method', () => {
    component.open();
    expect(modalService.open).toHaveBeenCalledWith(ProductCarousalComponent);
  });

});
