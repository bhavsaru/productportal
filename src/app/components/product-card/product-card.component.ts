import { Component, OnInit, Input } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import {ProductCarousalComponent} from "../product-carousal/product-carousal.component";

@Component({
  selector: 'app-product-card',
  templateUrl: './product-card.component.html',
  styleUrls: ['./product-card.component.scss']
})
export class ProductCardComponent implements OnInit {

  constructor(private modalService: NgbModal) { }

  @Input() product: any;

  ngOnInit() {
  }

  open() {
    const modalRef = this.modalService.open(ProductCarousalComponent);
    modalRef.componentInstance.product = this.product;
  }

}
