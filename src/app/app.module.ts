import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ProductCarousalComponent } from './components/product-carousal/product-carousal.component';
import { ProductCardComponent } from './components/product-card/product-card.component';
import { NgbActiveModal, NgbModule } from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  declarations: [
    AppComponent,
    ProductCarousalComponent,
    ProductCardComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule
  ],
  entryComponents: [ProductCarousalComponent],
  providers: [NgbActiveModal],
  bootstrap: [AppComponent]

})
export class AppModule { }
